#include <stdio.h>
#include "includeKDB.h"
using namespace kdb;

int main()
{
    int keyno = 10;
    char str[100] = "admin";

    databasexx *cdbptr;
    cdbptr = new databasexx();

    cout << " this databse function is called  by the namespace and included\n"
            "by the header file"
         << endl;
    cout << endl;

    cdbptr->find_rcd(keyno);
    cdbptr->include_rcd(keyno, str);


    return 0;
}