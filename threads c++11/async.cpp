#include <iostream>

#include <stdio.h>
#include <unistd.h>
#include <thread>
#include <chrono>
#include <future>

using namespace std;

int factorial(int no ) //, int &fact)
{
    int res = 1;
    for (int i = 1; i <= no; i++)
    {
        res *= i;
    }
    cout << "Factorial = " << res << endl;
    // fact = res;
    return res;
}

int main()
{
    int fact = 0; // it is used to get the factorial output int the main thread variable by reference , we can do
                  // thread t(factorial, 5, ref(fact)); // thread is created and , reference is used to get the output value;
                  //  cout << "main  value - \n" << fact;
                  // it does not print the value because if the line get executed before the thread exits then value will be zero
                  // so we need to use the async future here

    future<int> f = async(launch::async, factorial, 5);
    fact = f.get();
    cout << "main  value - " << fact << endl;
    

    // thread t(factorial, 5, ref(fact)); // thread is created and , the factorial function is given to thread for the execution.
    // t.join(); // joined the thread to main thread to complete the work of thread t ie-> to perform the factorial operation
    // cout << "main  value - " << fact; // if written here then we get the value as the operation is get completed because of join.

    return 0;
}