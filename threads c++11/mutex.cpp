/* Mutex: mutual exclusive

- Race Condition:
    0. Race condition is a situation where two or more threads/process
    happened to change a common data at the same time.
    1. If there is a condition then we have to protect it and the protected
    section is called critical section/region.

- MUTEX:
  0. Mutex is used to avoid race condition.
  1. we use lock(), unlock() on mutexx to avoid race condition.
u
uuuuuyufsdcxesdasawteju

*/
#include <iostream>
#include <thread>
#include <mutex>
using namespace std;

mutex m;

int myamt = 0;
void addMoney()
{
    m.lock();
    ++myamt;
    m.unlock();
}

int main()
{
    thread t1(addMoney);
    thread t2(addMoney);
    

    t1.join();
    t2.join();

    cout << myamt << endl;
    return 0;
}
