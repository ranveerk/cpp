#include <iostream>

#include <stdio.h>
#include <unistd.h>
#include <thread>
#include <chrono>
#include <future>

using namespace std;

void thread_function(int i);

// using namespace std::chrono_literals;

int main()
{
    cout << "main thread" << endl;
    thread t(&thread_function, 10);

    t.join(); // main threads waits for the thread t to finish;
    // cout << "sleep ended" << endl;

    return 0;
}

void thread_function(int i)
{
    cout << "in thread_function" << endl;
    cout << "i value - " << i << endl;
    // this_thread::sleep_for(chrono::milliseconds(2000));     // sleep time in mili-seconds
}
