#include <iostream>

#include <stdio.h>
#include <unistd.h>
#include <thread>
#include <chrono>
#include <future>

using namespace std;

int factorial(future<int> &f )
{
    int no = f.get();
    int res = 1;

    for (int i = 1; i <= no; i++)
    {
        res *= i;
    }
    cout << "Factorial = " << res << endl;    
    return res;
}

int main()
{
    int fact = 0; // it is used to get the factorial output int the main thread

    promise<int> prom;
    future<int> fp = prom.get_future();

    future<int> f = async(launch::async, factorial, ref(fp));

     prom.set_value(5);

    fact = f.get();
    cout << "main  value - " << fact << endl;

    // thread t(factorial, 5, ref(fact)); // thread is created and , the factorial function is given to thread for the execution.
    // t.join(); // joined the thread to main thread to complete the work of thread t ie-> to perform the factorial operation
    // cout << "main  value - " << fact; // if written here then we get the value as the operation is get completed because of join.

    return 0;
}