#include <iostream>
#include <thread>
#include <mutex>

using namespace std;

mutex m1, m2;

int data = 0;

void func1(int x)
{
    lock_guard <mutex> guard1(m1);
    cout << x << endl;

    lock_guard <mutex> guard2(m2);
    // data++;
}

void func2(int x)
{
    lock_guard<mutex> guard1(m2);
    cout << x<<endl;

    lock_guard<mutex> guard2(m1);
    // data++;
    // cout << data;
}

int main()
{
    thread t1(func1,1);
    // t1.join();

    func2(2);

    return 0;
}